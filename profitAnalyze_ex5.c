/** This program shows how total profit changes with the price of the ticket
 * of a movie theater
 */
#include<stdio.h>
#define baseCost 500         //base cost per a performance
#define costPerPartcpt 3     //cost per participant
#define foundTcktPrice 15    //price of the found
#define foundNoOfPartcpt 120

//Function prtotypes
int calPartcpt(int tcktPrice);
int calRevenue(int tcktPrice);
int calTotalCost(int tcktPrice);
int calprofit(int tcktPrice);

/** this function calculates the number of participants
 *  for a performance.
 *  According to the situation increasing of the
 *  ticket price by 5 decreses the participants by 20 and
 *  decreasing of ticket price by 5 increases the participants by
 *  20.
 *  And owner has found out for a price of 15 there are 120
 *  participants.
 */
int calPartcpt(int tcktPrice)
{
    return 120 - ((tcktPrice - 15) / 5 * 20);
}

/** this function calculates the total revenue for a performance.
 *      Total revenue = price of tickets x No. of participants
 */
int calRevenue(int tcktPrice)
{
    return tcktPrice * calPartcpt(tcktPrice);
}

/** this function calculates the total cost for a performance
 *      Total cost = base cost + (cost per a participant x
 *                                          no of participants)
 */
int calTotalCost(int tcktPrice)
{
    return (baseCost + (costPerPartcpt * calPartcpt(tcktPrice)));
}

/** this funcation calculates the profit
 *      profit = total revenue - total cost
 */
int calprofit(int tcktPrice)
{
    return (calRevenue(tcktPrice) - calTotalCost(tcktPrice));
}

//main function
int main()
{
    printf("\nProfit according to ticket price\n\n");
    printf("=================================\n");
    printf("| Ticket price\t | Profit\t| \n");
    printf("---------------------------------\n");

    /* this loop will iterate until participate count=0
     * and 
     * intial price of the ticket price has intialized 
     * to 5 as it is the lowest price for the ticket.
     */
    for (int price = 5; calPartcpt(price) >= 0; price += 5)
    {
        printf("| %d\t\t | %d\t\t| \n", price, calprofit(price));
    }

    printf("=================================\n\n");

    return 0;
}
